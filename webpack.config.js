const path = require('path'),
    webpack = require('webpack'),
    CopyWebpackPlugin = require('copy-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: {
    app: [
      path.resolve('./src/main/app.js'),
      path.resolve('./src/main/app.scss')
    ],
    vendors: [
      'angular',
      'angular-animate',
      'angular-aria',
      'angular-material',
      'angular-ui-router',
      'jjv'
    ]
  },
  devtool: 'source-map',
  output: {
    path: path.resolve('./build'),
    filename: 'bundles/app.[chunkhash].js'
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({fallback: "style-loader", use: ['css-loader', 'sass-loader']})
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({fallback: "style-loader", use: "css-loader"})
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({template: path.resolve(__dirname, 'src/main/index.ejs')}),
    new webpack.optimize.CommonsChunkPlugin({name: 'vendors', filename: 'bundles/vendors.[hash].js'}),
    new CopyWebpackPlugin([
      {context: path.resolve(__dirname, 'src/main'), from: '**/*.html', to: '.'},
      {context: path.resolve(__dirname, 'src/main'), from: '**/*.svg', to: '.'},
      {context: path.resolve(__dirname, 'src/main'), from: '**/*.ico', to: '.'}
    ]),
    new ExtractTextPlugin("[name].[chunkhash].css")
  ]
};